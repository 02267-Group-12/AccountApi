FROM maven:3.5-jdk-11 AS build  
COPY pom.xml /usr/src/app/pom.xml
RUN mvn -f /usr/src/app/pom.xml verify clean --fail-never
COPY src /usr/src/app/src  
RUN mvn -f /usr/src/app/pom.xml package


FROM adoptopenjdk:11-jre-hotspot
WORKDIR /usr/src
COPY --from=build /usr/src/app/target/quarkus-app /usr/src/quarkus-app
CMD java -Xmx64m -jar quarkus-app/quarkus-run.jar