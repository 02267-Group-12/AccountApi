Feature: Register a Customer Account feature

  Scenario: Register Customer Account
    When a "request-account-registration" event for a student is received
    Then the "account-registered" event is sent with the same correlation id
    And the student gets a student id
  