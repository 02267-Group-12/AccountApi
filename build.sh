image_name="account-api"
version=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)

docker build -t ${image_name}:${version} -t ${image_name}:latest .
