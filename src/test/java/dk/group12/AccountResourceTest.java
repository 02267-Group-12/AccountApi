package dk.group12;

import javax.enterprise.inject.Any;
import javax.inject.Inject;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Metadata;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import dk.group12.events.CreateAccount;
import dk.group12.events.Event;
import io.quarkus.test.common.ResourceArg;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.mutiny.Uni;
import io.smallrye.reactive.messaging.providers.connectors.InMemoryConnector;
import io.smallrye.reactive.messaging.providers.connectors.InMemorySink;
import io.smallrye.reactive.messaging.providers.connectors.InMemorySource;
import io.smallrye.reactive.messaging.rabbitmq.IncomingRabbitMQMetadata;
import io.smallrye.reactive.messaging.rabbitmq.OutgoingRabbitMQMetadata;

@QuarkusTest
@QuarkusTestResource(value = InMemReactiveMessagingLifecycleManager.class, initArgs = {
   @ResourceArg(value = "incoming", name = "request-account-registration"),
   @ResourceArg(value = "outgoing", name = "account-registered")
})
public class AccountResourceTest {

    @BeforeAll
    public static void switchMyChannels() {
        InMemoryConnector.switchIncomingChannelsToInMemory("request-account-registration");
        InMemoryConnector.switchOutgoingChannelsToInMemory("account-registered");
    }

    @AfterAll
    public static void revertMyChannels() {
        InMemoryConnector.clear();
    }

    @Inject
    @Any
    InMemoryConnector connector;

    @Inject
    AccountService aService;

    // This did not work with our final implementation :(
    // @Test
    // void test() {
    //     InMemorySource<Message<Event>> requestAccountRegistration = connector.source("request-account-registration");
    //     InMemorySink<Uni<Message<UUID>>> results = connector.sink("account-registered");

    //     final String correlationId = UUID.randomUUID().toString();
    //     final OutgoingRabbitMQMetadata metadata = new OutgoingRabbitMQMetadata.Builder()
    //             .withTimestamp(ZonedDateTime.now())
    //             .withCorrelationId(correlationId)
    //             .build();

    //     Message<Event> message = Message.of(new Event("example-topic", new Object[] {
    //      new CreateAccount(
    //       "cid1",
    //       "DKK0192309123",
    //       "Customer"
    //      )}), Metadata.of(metadata));

    //     // final Optional<IncomingRabbitMQMetadata> metadata = message.getMetadata(IncomingRabbitMQMetadata.class);
    //     // metadata.ifPresent(meta -> {
    //     //     final Optional<String> contentEncoding = meta.getContentEncoding();
    //     //     final Optional<String> contentType = meta.getContentType();
    //     //     final Optional<String> correlationId = meta.getCorrelationId();
    //     //     final Optional<ZonedDateTime> timestamp = meta.getTimestamp(ZoneId.systemDefault());
    //     //     final Optional<Integer> priority = meta.getPriority();
    //     //     final Optional<String> replyTo = meta.getReplyTo();
    //     //     final Optional<String> userId = meta.getUserId();

    //     //     final Map<String, Object> headers = meta.getHeaders();
    //     // });

    //     requestAccountRegistration.send(message);

    //     Assertions.assertEquals(1, results.received().size());
    // }
}

