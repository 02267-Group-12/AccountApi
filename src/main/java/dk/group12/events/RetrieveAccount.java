package dk.group12.events;

import java.util.UUID;

import lombok.Data;

@Data
public class RetrieveAccount extends Event {
  
  private static final long serialVersionUID = 4946537107527098982L;
  private String accountId = null;
  private String accountType = null;
  private UUID accountUUID = null;
}
