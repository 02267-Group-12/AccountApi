package dk.group12.events;

import java.util.UUID;

import lombok.Data;

@Data
public class DeleteAccount extends Event {
 
  private static final long serialVersionUID = -7384084066582779955L;
  private UUID accountUUID;

}
