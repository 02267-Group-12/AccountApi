package dk.group12.events;

import java.util.UUID;

import lombok.Data;

@Data
public class AccountCreated extends Event {
  
  private static final long serialVersionUID = 154031643258039739L;
  private UUID accountUUID;
  private String accountId;
  private String accountNumber;
  private String accountType;

}
