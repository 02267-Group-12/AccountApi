package dk.group12;

import java.util.Optional;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Metadata;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.jboss.logging.Logger;

import com.google.gson.Gson;

import dk.group12.events.CorrelationId;
import dk.group12.events.CreateAccount;
import dk.group12.events.Event;
import io.smallrye.mutiny.Uni;
import io.smallrye.reactive.messaging.rabbitmq.IncomingRabbitMQMetadata;
import io.smallrye.reactive.messaging.rabbitmq.OutgoingRabbitMQMetadata;
import io.smallrye.reactive.messaging.rabbitmq.OutgoingRabbitMQMetadata.Builder;
import io.vertx.core.json.JsonObject;

@ApplicationScoped
public class AccountResource {

  private static final Logger LOG = Logger.getLogger(AccountResource.class);

  @Inject
  AccountService aService;

  /**
   * @author Chris
   * @param obj
   * @return
   */
  @Incoming("request-account-registration")
  @Outgoing("account-registered")
  public Uni<Message<JsonObject>> registerAccount(Message<JsonObject> obj) {
    Optional<IncomingRabbitMQMetadata> inMeta = obj.getMetadata(IncomingRabbitMQMetadata.class);
    LOG.info(obj);
    Event ev = convertJsonToObject(obj.getPayload(), Event.class);
    LOG.info(ev.toString());
    CreateAccount account = ev.getArgument(0, CreateAccount.class);
    CorrelationId corrId = ev.getArgument(1, CorrelationId.class);
    LOG.infof("Received Request to register account: %s", account);
    LOG.infof("\t With correlationId of: %s", corrId.getId());

    Builder meta = OutgoingRabbitMQMetadata.builder();
    inMeta.ifPresent(
        m -> m.getReplyTo().ifPresent(
            r -> {
              if (r.equals("customer")) {
                meta.withRoutingKey("customer");
                LOG.info("Routing to customer");
              } else if (r.equals("merchant")) {
                meta.withRoutingKey("merchant");
                LOG.info("Routing to merchant");
              } else {}
            }));
    OutgoingRabbitMQMetadata finalMeta = meta.build();

    LOG.infof("Request to register account: %s", account);
    Uni<Message<JsonObject>> uni = Uni.createFrom().item(() -> aService.register(account)).log()
        .map(id -> new Event("account-registered", new Object[] { id, corrId })).log()
        .map(this::convertObjectToJson)
        .map(jObj -> Message.of(jObj, Metadata.of(finalMeta)).withAck(obj.getAck()));
    return uni;
  }

  // NOT USED
  // @Incoming("request-account-deregistration")
  // @Outgoing("account-deregistered")
  // public Uni<Void> deregisterAccount(DeleteAccount event) {
  //   return aService.deregister(event);
  // }

  /**
   * @author Chris
   * @param obj
   * @return
   */
  @Incoming("request-retrieve-account")
  @Outgoing("account-retrieved")
  public Uni<Message<JsonObject>> retrieveAccount(Message<JsonObject> obj) {
    Optional<IncomingRabbitMQMetadata> inMeta = obj.getMetadata(IncomingRabbitMQMetadata.class);
    LOG.info(obj);
    Event ev = convertJsonToObject(obj.getPayload(), Event.class);
    UUID requestUUID = ev.getArgument(0, UUID.class);
    CorrelationId corrId = ev.getArgument(1, CorrelationId.class);

    Builder meta = OutgoingRabbitMQMetadata.builder();
    inMeta.ifPresent(
        m -> m.getReplyTo().ifPresent(
            r -> {
              if (r.equals("payment")) {
                LOG.info("Routing to payment");
                meta.withRoutingKey("payment");
              } else if (r.equals("customer")) {
                meta.withRoutingKey("customer");
                LOG.info("Routing to customer");
              } else if (r.equals("merchant")) {
                meta.withRoutingKey("merchant");
                LOG.info("Routing to merchant");
              } else {}
            }));
    OutgoingRabbitMQMetadata finalMeta = meta.build();

    if (requestUUID != null) {
      return aService.retrieveByUUID(requestUUID)
          .map(account -> new Event("account-retrieved", new Object[] { account, corrId }))
          .map(this::convertObjectToJson)
          .map(jObj -> Message.of(jObj, Metadata.of(finalMeta)))
          .onItem().invoke(() -> obj.ack());
    }

    return Uni.createFrom().failure(new Exception("Invalid Request"));
  }

  private <T> T convertJsonToObject(JsonObject jObj, Class<T> classType) {
    return new Gson().fromJson(jObj.toString(), classType);
  }

  private JsonObject convertObjectToJson(Object obj) {
    return new JsonObject(new Gson().toJson(obj, obj.getClass()));
  }
}
