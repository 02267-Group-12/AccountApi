package dk.group12;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.context.ApplicationScoped;

import org.jboss.logging.Logger;

import dk.group12.events.CreateAccount;
import dk.group12.events.DeleteAccount;
import dk.group12.events.RetrieveAccount;
import io.smallrye.mutiny.Uni;

@ApplicationScoped
public class AccountService {

  private static final Logger LOG = Logger.getLogger(AccountService.class);

  private Map<UUID, Account> accountRepository = new ConcurrentHashMap<>();

  /**
   * @author Chris
   * @param accountUUID
   * @return
   */
  public Account getAccount(UUID accountUUID){
    return accountRepository.get(accountUUID);
  }

  /**
   * @author Chris
   * @param event
   * @return
   */
  public UUID register(CreateAccount event) {
    UUID account_uuid = UUID.randomUUID();
    Account account = new Account(account_uuid, event.getAccountId(), event.getAccountType(), event.getBankAccountId());
    LOG.debugf("Created new account with details: %s", account);
    accountRepository.put(account_uuid, account);
    return account_uuid;
  }

  // UNUSED
  // public Uni<Void> deregister(DeleteAccount event) {
  //   accountRepository.remove(event.getAccountUUID());
  //   return Uni.createFrom().voidItem();
  // }

  /**
   * @author Chris
   * @param uuid
   * @return
   */
  public Uni<Account> retrieveByUUID(UUID uuid) {
    return Uni.createFrom().item(accountRepository.get(uuid));
  }

  // public Uni<Account> retrieveByIdAndType(RetrieveAccount event) {
  //   for (Map.Entry<UUID, Account> set : accountRepository.entrySet()) {
  //     Account acc = set.getValue();
  //     if (acc.getAccountId() == event.getAccountId() && acc.getAccountType() == event.getAccountType()) {
  //       return Uni.createFrom().item(acc);
  //     }
  //   }
  //   return Uni.createFrom().failure(new Exception("No Account Found"));
  // }

}
